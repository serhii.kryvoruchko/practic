import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, delay } from 'rxjs/operators'

export interface Todo {
   complited: boolean,
   title: string,
   id?: number
}
@Injectable({
   providedIn: 'root'
})
export class TodoService {

   constructor(private http: HttpClient) { }

   addPost(todo: Todo): Observable<Todo> {
      return this.http.post<Todo>('https://jsonplaceholder.typicode.com/todos', todo);
   }

   downloadTodo(): Observable<Todo[]> {
      return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos?_limit=3')
         .pipe(
            delay(1500)
         )
   }

   removeTodo(id: number): Observable<void> {
      return this.http.delete<void>(`https://jsonplaceholder.typicode.com/todos/${id}`)
   }
   complitedTodo(id: number): Observable<Todo> {
      return this.http.put<Todo>(`https://jsonplaceholder.typicode.com/todos/${id}`, {
         complited: true
      })
         .pipe(
            catchError(error => {
               alert("Error")
               return throwError(error)
            })
         )
   }
}
