import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
   selector: 'appModalBox',
   templateUrl: './modal-box.component.html',
   styleUrls: ['./modal-box.component.scss']
})
export class ModalBoxComponent implements OnInit {
   @Input() title = 'Default title'
   @Output() close = new EventEmitter()
   constructor() { }

   ngOnInit(): void {
   }

}
