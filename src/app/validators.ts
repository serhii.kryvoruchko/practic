import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";

export class MyValidators {
   static wrongEmails(controls: FormControl): { [key: string]: boolean } | null {
      if (['v.mail.ru', '12@12.1'].includes(controls.value)) {
         return {
            'wrongEmail': true
         }
      }
      return null
   }


   static basyEmail(controls: FormControl): Promise<any> | Observable<any> {
      return new Promise(resolve => {
         setTimeout(() => {
            if (controls.value === 'async@mail.com') {
               resolve({ basyEmail: true })
            }
            else {
               resolve(null)
            }
         }, 2000)
      })
   }
}