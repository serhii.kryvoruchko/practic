import { Injectable } from '@angular/core';
import {
   Router, Resolve,
   RouterStateSnapshot,
   ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Post, PostsService } from './posts.service';

@Injectable({
   providedIn: 'root'
})
export class PostResolver implements Resolve<Post>{
   constructor(private postService: PostsService) { }
   resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Post | Observable<Post> | Promise<Post> {
      throw new Error('Method not implemented.');
      // return of(this.postService.getById(+route.params['id']))
      //    .pipe(
      //       delay(1500)
      //    )
   }


}
