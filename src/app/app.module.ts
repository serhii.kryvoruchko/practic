import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FormComponent } from './form/form.component';
import { PostsComponent } from './posts/posts.component';
import { ModalComponent } from './modal/modal.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { PostComponent } from './post/post.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { RoutingModule } from './routing/routing.module';
import { PostsExtraComponent } from './posts-extra/posts-extra.component';
import { ModalBoxComponent } from './modal-box/modal-box.component';
import { MyValidators } from './validators';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';
import { counterReducer } from './counter/counter.reducer';
import { CounterComponent } from './counter/counter.component';

@NgModule({
   declarations: [
      AppComponent,
      HomeComponent,
      FormComponent,
      PostsComponent,
      ModalComponent,
      ErrorPageComponent,
      PostComponent,
      PostsExtraComponent,
      ModalBoxComponent,
      CounterComponent,
   ],
   imports: [
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,
      RoutingModule,
      StoreModule.forRoot({ count: counterReducer }),
      StoreDevtoolsModule.instrument({
         maxAge: 25,
         logOnly: false,
         autoPause: true,
         features: {
           pause: false,
           lock: true,
           persist: true
         }
       })
   ],
   providers: [],
   bootstrap: [AppComponent]
})
export class AppModule { }
