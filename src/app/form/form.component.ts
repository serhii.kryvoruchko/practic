import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { MyValidators } from '../validators';

@Component({
   selector: 'app-form',
   templateUrl: './form.component.html',
   styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
   form!: FormGroup

   constructor() { }

   ngOnInit(): void {
      this.form = new FormGroup({
         email: new FormControl('', [Validators.required, Validators.email, MyValidators.wrongEmails]),
         password: new FormControl('', [Validators.required, Validators.minLength(8)]),
         address: new FormGroup({
            country: new FormControl('ua'),
            city: new FormControl('Киев', Validators.required)
         }),
         skills: new FormArray([])

      })
   }
   get skillControl() {
      return (this.form.get('skills') as FormArray).controls
   }
   submit() {
      const formData = { ...this.form.value }
      console.log('Form:', formData);
   }
   setCapital() {
      const cityMap: any = {
         ru: 'Москва',
         by: 'Минск',
         ua: 'Харьков'
      }
      const cityKey = this.form.get('address')?.get('country')?.value;
      const city = cityMap[cityKey];

      this.form.patchValue({
         address: { city: city }
      })
   }
   addSkill() {
      const control = new FormControl('', Validators.required);
      (this.form.get('skills') as FormArray).push(control)
   }

}


