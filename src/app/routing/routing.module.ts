import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { PostsComponent } from '../posts/posts.component';
import { ModalComponent } from '../modal/modal.component';
import { FormComponent } from '../form/form.component';
import { ErrorPageComponent } from '../error-page/error-page.component';
import { PostsExtraComponent } from '../posts-extra/posts-extra.component';
import { PostComponent } from '../post/post.component';
import { PostsService } from '../posts.service';
import { PostResolver } from '../post.resolver';

// http://localhost:59730/ -> HomeComponent
// http://localhost:59730/posts -> PostsComponent 
// http://localhost:59730/posts/extra -> PostsComponent 
// http://localhost:59730/modal -> ModalComponent
// http://localhost:59730/login -> FormlComponent
// http://localhost:59730/error -> ErrorPageComponent 

const appRoutes: Routes = [
   { path: '', component: HomeComponent },
   {
      path: 'posts', component: PostsComponent, children: [
         { path: 'extra', component: PostsExtraComponent },


      ]
   },
   { path: 'posts/:id', component: PostComponent },
   { path: 'modal', component: ModalComponent },
   { path: 'login', component: FormComponent },
   { path: '**', component: ErrorPageComponent },
]

@NgModule({
   declarations: [],
   imports: [
      // CommonModule,
      RouterModule.forRoot(appRoutes)
   ],
   exports: [RouterModule]
})
export class RoutingModule { }
