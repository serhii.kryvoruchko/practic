import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Todo, TodoService } from '../todo.service';


@Component({
   selector: 'app-home',
   templateUrl: './home.component.html',
   styleUrls: ['./home.component.scss']
})
export class HomeComponent {

   todos: Todo[] = []
   title = 'practicTime';
   todoTitle = ''
   loading = false
   error = ''

   constructor(private todoService: TodoService, private rout: Router) { }
   addTodo() {
      if (!this.todoTitle.trim()) {
         return
      }

      this.todoService.addPost({
         title: this.todoTitle,
         complited: false
      }).subscribe(todo => {
         this.todos.push(todo)
         this.todoTitle = ''
      })
   }


   delTodo(id: number) {
      this.todoService.removeTodo(id).subscribe(() => {
         this.todos = this.todos.filter(t => t.id !== id)
      })
   }

   downloadTodo() {
      this.loading = true
      this.todoService.downloadTodo().subscribe(todos => {

         this.todos = todos
         this.loading = false
      })
   }

   complitedTodo(id: number) {
      this.todoService.complitedTodo(id).subscribe(todo => {
         this.todos.find(t => t.id == todo.id)!.complited = true
      }, errors => {
         console.log("переменная :", this.error);
         this.error += errors.message

         console.log('Ошибка', errors);
         console.log("переменная :", this.error);

      })
   }
   goToPosts() {
      this.rout.navigate(['/posts'])
   }
}
