import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Post, PostsService } from '../posts.service';


@Component({
   selector: 'app-post',
   templateUrl: './post.component.html',
   styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
   post!: Post
   constructor(public postsService: PostsService, private route: ActivatedRoute) { }

   ngOnInit(): void {
      // this.post = this.route.snapshot.params.data.post
      // --------------------------------------------------------------------

      this.route.data.subscribe(data => {
         this.post = data.post
      })
      //  this.route.params.subscribe(p => {
      //    console.log("Here", p)
      //    this.post = this.postsService.getById(+p.id)
      // })
   }

}
